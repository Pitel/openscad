difference() {
    linear_extrude((80 + 55) / 2 + 2)
    polygon([
        [-32.5 * 1.5, 0],
        [32.5 * 1.5, 0],
        [32.5 * 1.5, 2],
        [5, 32.5],
        [-5, 32.5],
        [-32.5 * 1.5, 2]
    ]);

    #translate([0, 15.5 / 2 + 2, 2])
        linear_extrude((80 + 55) / 2)
            hull() {
                circle(d = 15.5);
                translate([0, 28.5 - 15.5, 0]) circle(d = 15.5);
            }
}