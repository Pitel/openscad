difference() {
    linear_extrude(135) polygon([
        [85 / 2, 0],
        [15 / 2, 55],
        [-15 / 2, 55],
        [-85 / 2, 0]
    ]);
    rotate([-5]) translate([-85 / 2, 10, 20]) union() {
        cube([85, 5.5, 135]);
        translate([0, -135, 10])cube([85, 135, 135]);
    }
}