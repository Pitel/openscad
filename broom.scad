wall=1.27;
d=21.75;

difference() {
    cylinder(d = d + 2 * wall, h = d + wall);
    cylinder(d = d, h = d);
}