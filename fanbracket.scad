wall = 2.46;

module holes() {
    square([81 - wall, 81 - wall], true);
    translate([0, 50]) circle(d = 4);
    translate([0, -50]) circle(d = 4);
    translate([50, 0]) circle(d = 4);
    translate([-50, 0]) circle(d = 4);
}

difference() {
    union() {
        translate([0, 0, (25 + wall) / 2]) cube([81 + 2 * wall, 81 + 2 * wall, 25 + wall], true);
        linear_extrude(wall) difference() {
            hull() offset(2 * wall) holes();
            holes();
        }
    }
    translate([0, 0, 25 / 2]) cube([81, 81, 25], true);
    linear_extrude(25 + wall) hull() {
        translate([81 / 2 - 20, 81 / 2 - 20]) circle(20);
        translate([-81 / 2 + 20, 81 / 2 - 20]) circle(20);
        translate([-81 / 2 + 20, -81 / 2 + 20]) circle(20);
        translate([81 / 2 - 20, -81 / 2 + 20]) circle(20);
    }
    //Translatecylinder(r = 18, h = 25 + wall);
}