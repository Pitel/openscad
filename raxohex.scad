wall = 2.46; // 4 perimeters, 0.4 layer, 0.6 nozzle
$fn = 6;

difference() {
    cylinder(d = 180, h = 30 + wall); // outer
    translate([0, 0, wall])
        cylinder(d = 180 - 2 * wall, h = 30); // inner
}