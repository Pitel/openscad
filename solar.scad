a=54;
t=3.25;

difference() {
    cube([2*a+3*t, a+2*t, 1.5*t]);
    
    translate([t, t, 0.5*t]) cube([a, a, t]);
    translate([t, 2*t]) cube([a, a-2*t, t]);
    
    translate([2*t+a, t, 0.5*t]) cube([a, a, t]);
    translate([2*t+a, 2*t]) cube([a, a-2*t, t]);
}