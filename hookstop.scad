rod = 28;
wall = 2;
length = rod;
height = rod / 2;
screw = 4.5;

difference() {
    cylinder(d = rod + 2 * wall, h = height);
    translate([0, 0, wall])
        cylinder(d = rod, h = height);
    translate([-(rod + 2 * wall) / 2, 0, 0])
        cube(rod + 2 * wall);
}
translate([rod / 2, 0, 0]) difference() {
    translate([0, -rod, 0])
        cube([wall, length + rod, height]);
    rotate([0, 90]) {
        translate([-height / 2, length / 3])
            cylinder(d = screw, h = wall);
        translate([-height / 2, 2 * length / 3])
            cylinder(d = screw, h = wall);
        translate([-height / 2, -rod * 0.75])
            cylinder(d = screw, h = wall);
    }
}