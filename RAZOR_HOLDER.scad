difference() {
    translate([-30, -10, 0]) import("RAZOR_HOLDER.stl");
    scale([1.8, 1, 1]) difference() {
        translate([-11, -5, 0]) cube([20, 20, 24]);
        translate([-30, -10, 0]) import("RAZOR_HOLDER.stl");
    }
}