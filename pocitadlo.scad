$fs = 0.2;

difference() {
    linear_extrude(10)
    scale(0.3255)
    translate([24, 410])
    rotate(180 + 43)
    import("pocitadlo.svg");
    
    for(i = [0:10]) {
        translate([10 + i * 4, 150 - i * 14, 2])
        cylinder(d = 2, h = 10);
    }
}