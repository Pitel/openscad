difference() {
    linear_extrude(2) difference() {
        hull() {
            translate([-6, 0]) circle(d = 20);
            translate([6, 0]) circle(d = 20);
        }
        translate([-10, 0]) circle(d = 2);
        translate([10, 0]) circle(d = 2);
    }
    translate([0, 0, 0.2]) cube([10.5, 50, 0.4], true);
}