//$fn = 90;
wall = 1.3; // 0.6 nozzle, 0.3 layer
inner_diameter = 39 + 0.5;
inner_height = 7.5;

difference() {
    union() {
        cylinder(d = inner_diameter + 2 * wall, h = inner_height + wall); // outer
        translate([inner_diameter / 2, 0, inner_height + wall])
            sphere(d = inner_height + wall / 2, $fn = 30);
    }
    translate([0, 0, wall])
        cylinder(d = inner_diameter, h = inner_height); // inner
    translate([0, 0, wall + inner_height])
        cylinder(d = inner_diameter * 2, h = inner_diameter * 2); // up, for holder
}