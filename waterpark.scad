top = 69;
bottom = 56;
height = 52; // Just the ditch!
handle = 30;
r = 7;

linear_extrude(7.5) hull() {
    translate([-top / 2, height]) square([top, handle]);
    translate([bottom / 2 - r, r]) circle(r);
    translate([-bottom / 2 + r, r]) circle(r);
}