module half() {
    polygon([
        [0, 0],
        [0, 43],
        [27, 43],
        [27, 43 + 50],
        [90, 0]
    ]);
}

linear_extrude(180) {
    half();
    mirror([1, 0, 0]) half();
}