cube([160, 40 + 20, 2]);
for(x = [20 : 40 : 160]) {
    translate([x, 40 + 10, 2]) cylinder(h = 7, d = 8);
}