module holes() {
    square([19.5, 12.3], true);
    translate([0, 15]) circle(d = 4);
    translate([0, -15]) circle(d = 4);
}

linear_extrude(2) difference() {
    hull() offset(3) holes();
    holes();
}