d1 = 87;
d2 = 38;
h = 10;

difference() {
    cube([d1 + h, d1 + h, 2 * h]);
    translate([h / 2, d1 / 2 + h / 2, h / 2])
        union() {
            translate([d1 / 2, 0, 0])
                cylinder(h, d = d1);
            cube([d1, d1 / 2 + h / 2, h]);
            translate([d1 / 2 - d2 / 2, 0, h]) {
                translate([d2 / 2, 0, 0])
                    cylinder(h, d = d2);
                cube([d2, d1 / 2 + h / 2, h]);
            }
        }
}