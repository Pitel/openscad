translate([0, 0, 12]) difference() {
    cube([79 + 2 * 2, 50, 14 + 2 * 2]);
    translate([2, 0, 2]) difference() {
        cube([79, 50, 14]);
        cube([79, 25, 3]);
    }
    translate([4, 0, 16]) cube([79 - 2 * 2, 50, 2]);
    translate([79 + 2 * 2 - (2 + 20), 25 + 12.5]) cylinder(d = 15, h = 20);
}
translate([79 + 2 * 2 - (2 + 20), 25 + 12.5]) difference() {
    cylinder(d=35.5, h=12);
    cylinder(d=35.5 - 2 * 2, h=12);
}