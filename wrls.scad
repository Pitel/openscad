difference() {
    union() {
        translate([0, 0, 24.2])import("WRLS_Inlet_Tubeless_Presta_Metric.stl");
        rotate_extrude() translate([10, 0, 0]) circle(d = 5);
        cylinder(d=18, h=15);
    }
    translate([-25, -25, -50]) cube(50);
    rotate_extrude() translate([10, 0, 0]) circle(d = 2.5);
    cylinder(d=8, h=20);
}