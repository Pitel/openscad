$fs = 0.1;

difference() {
    cube([34.5, 61, 15.5], true);
    translate([0, 61 / 2 - 10.15 / 2 - 10.25, 0])
        cylinder(h = 16, d = 10.15, center = true);
    translate([0, -(61 / 2 - 10.15 / 2 - 10.25), 0])
        cylinder(h = 16, d = 10.15, center = true);
}