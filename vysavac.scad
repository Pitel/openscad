for(i = [0:5]) {
    translate([0, 0, 7 * i]) difference() {
        cylinder(d=26.5 + 2, h=7);
        cylinder(d1=26.5, d2=21, h=7/2);
        translate([0, 0, 7/2]) cylinder(d1=21, d2=26.5, h=7/2);
    }
}