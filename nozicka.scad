difference() {
    cylinder(d=7.5*3, h=7.5);
    translate([0, 0, 7.5 / 2 + 1]) cube([7.5, 30, 7.5], true);
}