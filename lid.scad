difference() {
    translate([0, 0, -100]) import("Teabox_60_round_lid.stl");
    cylinder(d=1, h=100, center=true);
    for (a=[0:45:360]) {
        rotate([0, 0, a])
        for (i=[5:5:20]) {
            translate([0, i]) cylinder(d=2, h=100, center=true);
        }
    }
}