wall=2.53;

w=113.5;
h=20;

tab=30;
height=30;
height2=40;
screw=5.5;

difference() {
    linear_extrude(height) difference() {
        square([w + 2 * (wall + tab), h + wall]);
        translate([0, wall])
            square([tab, h]); 
        translate([tab + wall, 0])
            square([w, h]); 
        translate([tab + w + 2 * wall, wall])
            square([tab, h]);
    }
    translate([tab / 2, 0, height / 2])
        rotate([-90])
            cylinder(d=screw, h=wall);
    translate([tab + wall + w + wall + tab / 2, 0, height / 2])
        rotate([-90])
            cylinder(d=screw, h=wall);
}



translate([0, h + 2 * wall])
difference() {
    linear_extrude(height2) difference() {
        square([wall + tab + 10, h + wall]);
        translate([0, wall])
            square([tab, h]); 
        translate([tab + wall, 0])
            square([w, h]); 
    }
    translate([tab / 2, 0, height2 / 2])
        rotate([-90])
            cylinder(d=screw, h=wall);
}