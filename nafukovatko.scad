difference() {
    union() { 
        cylinder(h=24, d1=6.4, d2=7.1);
        translate([0, 0, 24]) cylinder(h=3, d=9);
        translate([0, 0, 24 + 3]) cylinder(h=8, d=7.6);
    }
 cylinder(h=300, d=3.75);
}