wall=1.27;
x=52;
y=11;
z=45;
n=12;

difference() {
    cube([x + 2 * wall, y * n + wall * (n + 1), z + wall]);
    for(i=[0:n-1]) {
        translate([wall, wall + i * (wall + y), wall])
            cube([x, y, z]);
    }
}