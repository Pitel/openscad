difference() {
    import("wheel_holder.stl");
    translate([25/2, 25/2]) cylinder(d = 4, h = 10);
    translate([-25/2, 25/2]) cylinder(d = 4, h = 10);
    translate([25/2, -25/2]) cylinder(d = 4, h = 10);
    translate([-25/2, -25/2]) cylinder(d = 4, h = 10);
}