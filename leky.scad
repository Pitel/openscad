wall=1.27;
x=21;
y=13;
z=10;
n=7;
lidz=3;

//difference() {
//    cube([x + 2 * wall, y * n + wall * (n + 1), z + wall + lidz]);
//    for(i=[0:n-1]) {
//        translate([wall, wall + i * (wall + y), wall])
//            cube([x, y, z]);
//    }
//    #translate([wall, n * (y + wall), z + wall])
//        rotate([90, 0, 0])
//        linear_extrude(n * (y + wall))
//            polygon([
//                [0, 0],
//                [x, 0],
//                [x - 0.5, lidz],
//                [0.5, lidz]
//            ]);
//}

rotate([90, 0, 0])
    linear_extrude(n * (y + wall))
        polygon([
            [0, 0],
            [x, 0],
            [x - 0.5, lidz],
            [0.5, lidz]
        ]);
translate([2 * wall, -10, lidz])
    cube([x - 4 * wall, 2 * wall, wall]);