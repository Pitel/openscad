difference() {
    union() {
        sphere(d=41);
        linear_extrude(2) difference() {
            hull() {
                translate([49 / 2, 0]) circle(d=8);
                translate([-49 / 2, 0]) circle(d=8);
            }
            translate([49 / 2, 0]) circle(d=3.5);
            translate([-49 / 2, 0]) circle(d=3.5);
        }
        translate([0, 0, 40 / 2]) rotate([90, 0, 90]) difference() {
            cylinder(d=8, h=8, center=true);
            cylinder(d=6, h=8, center=true);
        }
    }
    sphere(d=37);
    translate([0, 0, -25]) cube(50, true);
}