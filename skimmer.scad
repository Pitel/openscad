linear_extrude(50) {
    r_out = 40;
    difference() {
        hull() {
            circle(r = r_out);
            translate([0, -r_out / 2]) circle(r = r_out);
        }
        translate([6, 0]) union() {
            hull() {
                translate([1, 0]) circle(d=22.5);
                translate([-11.5, -(22.5 - 5) / 2]) circle(d=5);
                translate([-23, (22.5 - 10) / 2]) circle(d=10);
            }
            translate([-7, -r_out * 2 +10]) square([2, r_out * 2]);
        }
    }
    translate([-r_out, -100]) square([17.5 + 2.75 * 2, 95]);

    translate([0, -r_out * 2.5 + 23]) difference() {
        translate([-r_out, 0]) square([r_out - 1, r_out]);
        circle(r = r_out - 23);
    }
}

translate([-40, -100 - 31 - 2.75]) {
    difference() {
        d=2.75;
        cube([17.5 + d * 2, 31 + d, 50]);
        translate([d, 0, (50-35.5)/2]) {
            cube([17.5, 31, 35.5]);
            translate([-(30 - 17.5) / 2, 31 - 15/2, 35.5/2]) rotate([0, 90])
                cylinder(h=30, d=15);
        }
    }
}