module cylinder_outer(height,radius,fn){
   fudge = 1/cos(180/fn);
   cylinder(h=height,r=radius*fudge,$fn=fn);}

difference() {
    cube([32, 20, 20]);
    translate([0, 0, 2]) cube([30, 18, 18]);
    translate([15, 10]) cylinder(d=2, h=20);
    translate([15, 0, 10]) rotate([-90]) cylinder(d=2, h=20);
}

translate([32, 10, 10]) rotate([45/2]) rotate([0, 90]) difference() {
    cylinder_outer(180-32, 10, 8);
    rotate_extrude($fn=8) translate([10, 120, 0]) rotate(45/2) circle(d=4, $fn=8);
}