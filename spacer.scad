d = 6;
h = 50;

rotate([90, 0, 0])
difference() {
    //cube([3 * d, h, 3 * d], center = true);
    cylinder(d = 3.5 * d, h = h, $fn = 6);
    cylinder(d = d + 1, h = 15);
    translate([0, 0, h - 15]) cylinder(d = d + 1, h = 15);
    translate([-10.5 / 2, -10.5 / 2, 10 - 3.5 / 2]) cube([10.5, 100, 3.5]);
    translate([-10.5 / 2, -10.5 / 2, h - 10 - 3.5 / 2]) cube([10.5, 100, 3.5]);
}