union() {
    difference() {
        translate([0, 0, 81.5 - 1.5])cube([45, 15, 3], true);
        translate([6.5 + 5.5 / 2 + 10 / 2, 0, 81.5 - 3])
            cylinder(d1 = 8.5, d2 = 5.5, h = 3);
        translate([-(6.5 + 5.5 / 2 + 10 / 2), 0, 81.5 - 3])
            cylinder(d1 = 8.5, d2 = 5.5, h = 3);
    }
    difference() {
        translate([-20 / 2, -15 / 2, 0]) cube([20, 15, 81.5]);
        translate([0, 20, 8 + 2]) rotate([90, 0, 0]) hull() {
            cylinder(d = 16, h = 50);
            translate([0, 16, 0]) cylinder(d = 16, h = 50);
        }
    }
}