$fn = 2 * 16;
union() {
    cylinder(d = 50);
    translate([0, 0, 14]) difference() {
        cylinder(2, d = 80);
        cylinder(2, d = 80 - 2 * 2);
    }
    
    for (i = [0 : 16])
        rotate([0, 0, i * (360 / 16)])
            translate([0, -24, 0.5])
                rotate([90 + 48, 0, 0])
                    linear_extrude(1, center = true)
                        polygon([[-5, 0], [5, 0], [0, 21]]);
    
    for (i = [0 : 8])
        rotate([0, 0, i * (360 / 8)])
            translate([0, 39, 15.5])
                rotate([90+31.25, 0, 0])
                    linear_extrude(1, center = true)
                        polygon([[-5, 0], [5, 0], [0, 75.5]]);
    
    //translate([0, 0, 80]) cube(center = true);
}


/*
$fs = 0.25;
difference() {
    cube([40.75, 25.9, 3.25], true);
    translate([2, 0, 0])
        linear_extrude(3.25, center = true, convexity = 1, slices = 1, scale = [1.1, 2])
            square([29.35, 3.5], true);
    mirror([0, 1, 0])
        translate([0, 25.9 / 2, 0])
            rotate([0, 90, 0])
                cylinder(40.75, d = 3.25 / 2.5, center = true);
    translate([0, 25.9 / 2, 0])
        rotate([0, 90, 0])
            cylinder(40.75, d = 3.25 / 2.5, center = true);
}
*/