linear_extrude(3) difference() {
    hull() {
        circle(33.33 / 2);
        translate([-33.33 / 2, 24 / 2]) square([33.33, 21]);
    }
    circle(24 / 2);
}
translate([-33.33 / 2, 24 / 2 + 21 - 6.66]) difference() {
    cube([33.33, 6.66, 36]);
    translate([33.33 / 2, -1, 7.5]) rotate([-90]) cylinder(d=4, h = 100);
    translate([33.33 / 2, -1, 21]) rotate([-90]) cylinder(d=4, h = 100);
}

translate([33.33 / 2, 24 / 2 + 21]) rotate([0, -90]) linear_extrude(5) polygon([[0, 0], [0, -21], [21, 0]]);
translate([-33.33 / 2 + 5, 24 / 2 + 21]) rotate([0, -90]) linear_extrude(5) polygon([[0, 0], [0, -21], [21, 0]]);