use <MCAD/regular_shapes.scad>

difference() {
    union() {
        linear_extrude(3.3) egg_outline(27.85 + 6, 47.5 + 6);
        
        translate([-2, 0, 2.3])
            cube([4, 47.5 + 6, 3]);
        
        difference() {
            union() {
                translate([0, 43, 0])
                    rotate([90])
                        cylinder(d = 24, h = 2);
                
                translate([0, 30, 0])
                    rotate([90])
                        cylinder(d = 32, h = 2);
                
                translate([0, 10, 0])
                    rotate([90])
                        cylinder(d = 28, h = 2);
            }
            
            translate([-(27.85 + 6) / 2, 0, -50 + 2.3])
                cube([27.85 + 6, 47.5 + 6, 50]);
        }
    }
    
    translate([-0.5, 0, 2.3])
            cube([1, 47.5 + 6, 1]);
}