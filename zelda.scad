//cylinder(d=50, h=10);
//translate([0, 0, 10]) linear_extrude(2) text("S", 15, halign="center", valign="center");
linear_extrude(10) offset(5) resize([48], auto = true) import("zelda.svg", center = true);
linear_extrude(12) offset(0.00001) resize([48], auto = true) import("zelda.svg", center = true);