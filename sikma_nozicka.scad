difference() {
    hull() {
        linear_extrude(0.00000000000000000000001) offset(5) square([20, 5]);
        translate([0, 0, 9]) linear_extrude(1) square([20, 5]);
    }
    difference() {
        translate([0, 0, 10]) rotate([0, 10]) translate([0, 0, -10]) cube([20, 5, 20]);
        translate([-5, -5, -8]) cube([30, 15, 10]);
    }
}