use <MCAD/2Dshapes.scad>

//intersection() {
difference() {
    linear_extrude(33) roundedSquare([120, 120], 30);
    
    translate([0, 0, 5]) linear_extrude(30) difference() {
        hull() {
            translate([(101 - 23) / 2, 0]) circle(d = 23);
            translate([-(101 - 23) / 2, 0]) circle(d = 23);
        }
        square(66, true);
    }
    
    translate([0, 25, 60]) rotate([0, 90]) cylinder(d=85, h=40, center=true);
}

//translate([(66 + 27 / 2) / 2, 0, 0]) cube([27, 27, 27*3], true);
//}