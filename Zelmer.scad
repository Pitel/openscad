use <MCAD/involute_gears.scad>;

$fs = 0.5;

difference() {
    union() {
        cylinder(20 + 5.5 + 1.5 + 8 + 4 + 2 + 5.5, d = 11);
        cylinder(5.5 + 2 + 4 + 8 + 1.5 + 5.5, d = 16);
        translate([0, 0, 5.5]) cylinder(2 + 4 + 8 + 1.5, d = 18.7);
        translate([0, 0, 5.5 + 2]) resize([38, 38, 4]) rotate(360 / (42 * 2)) gear (
            number_of_teeth = 42,
            diametral_pitch = 1,
            gear_thickness = 4,
            rim_thickness = 4,
            hub_thickness = 4,
            bore_diameter = 0
        );
        translate([0, 0, 5.5 + 2 + 4]) resize([32, 32, 8]) gear (
            number_of_teeth = 42,
            diametral_pitch = 1,
            gear_thickness = 8,
            rim_thickness = 8,
            hub_thickness = 8,
            bore_diameter = 0,
            twist = -8
        );
    }
    union() {
        cylinder(50, d = 4.7);
        translate([-8 / 2, -1.8 / 2, 4.5]) cube([8, 1.8, 50 - 4.5]);
        difference() {
            translate([0, 0, 4.5]) cylinder(25, d = 8);
            union() {
                cube([4, 4, 18 + 12]);
                translate([-4, -4, 0]) cube([4, 4, 18 + 12]);
            }
        }
    }
}