linear_extrude(7.5) difference() {
    square([54, 45.5]);
    
    // Power
    translate([9.5, 0]) square([9, 7.5]);
    
    // Left capacitor
    translate([3.5+6/2, 11]) circle(d=6);
    
    // Right capacitor
    translate([38+6/2+1/3, 14.5+5/2+1/3]) circle(d=6);
    
    // Reset
    translate([0, 26.5]) square([2, 99]);
    
    // Servo
    translate([0, 41.5]) square([8, 99]);
}