difference() {
    linear_extrude(6.5) polygon([
        [25.25 / 2, 0],
        [19    / 2, 33.33],
        [-19   / 2, 33.33],
        [-25.25 / 2, 0],
    ]);
    translate([0, 21]) cylinder(d=15, h=3.75);
    translate([-12, 30, 2]) cube([25, 5, 8]);
    translate([0, 5.5, 3]) cylinder(d1=4.5, d2=8, h=3.5);
    translate([0, 5.5, 0]) cylinder(d=4.5, h=3);
}