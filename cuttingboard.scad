difference() {
    resize([147.5, 150, 80]) rotate([0, 90]) cylinder(d=150, center=true);
    translate([0, 0, -100]) cube(200, center = true);
    translate([-5, 0, 0]) for(x = [-80 / 2 : 22.5 + 10 : 80 / 2]) {
        translate([x, -150 / 2, 3]) cube([22.5, 200, 200]);
    }
    translate([147.5/2-20, -150 / 2, 3]) cube([10, 200, 200]);
    translate([-147.5/2+10, -150 / 2, 3]) cube([10, 200, 200]);
}