difference() {
    cylinder(r = 17, h = 18, center = true);

    rotate_extrude()
    translate([20, 0, 0])
    rotate(90)
    circle(d = 12);    
    
    translate([0, 0, -10]) cube(20);
}