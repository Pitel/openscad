wall = 1.31;
dimension = "inside"; // [inside, outside]
height = 20.0; // [180]
depth = 35.0; // [180]
width = 50.0; // [180]
dimple = false;
lid = false;
compartments = 1;
shorter = 0; // [90]

/* [Hidden] */
//$fs=0.25;

module box() {
    difference() {
        cube([width + 2 * wall, depth + 2 * wall, height + wall], true);
        translate([0, 0, wall]) cube([width, depth, height + wall], true);
    }
}

module compartments(num = 2) {
    if (num > 1) {
        intersection() {
            cube([width + 2 * wall, depth + 2 * wall, height + wall], true);
            union() {
                for (i = [1 : num]) {
                    rotate([0, 0, i * (360 / num)])
                        translate([0, - wall / 2, -height])
                            cube([width * 2, wall, height * 2]);
                }
            }
        }
    }
}

module dimples(inside = false) {
    off = inside ? 0 : wall;
    translate([width / 2 + off, 0, 0]) sphere(d=2);
    translate([-width / 2 - off, 0, 0]) sphere(d=2);
    translate([0, depth / 2 + off, 0]) sphere(d=2);
    translate([0, -depth / 2 - off, 0]) sphere(d=2);
}

difference() {
    if (lid) {
        difference() {
            union() {
                box();
                compartments(compartments);
            }

            if (dimple) {
                dimples(lid);
            }
        }
    } else {
        union() {
            for (i = [0 : 3]) {
                translate([(width + wall) * i, 0, 0]) box();
            }
            compartments(compartments);

            if (dimple) {
                dimples(lid);
            }
        }
    }
    
    translate([0, 0, (height + wall) / 2 - shorter / 2])
        cube([width + depth, width + depth, shorter], true);
}