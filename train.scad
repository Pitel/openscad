linear_extrude(7.8) {
    difference() {
        square([10, 26]);
        translate([(10 - 3) / 2, 26 - 15.5]) square([3, 15.5]);
    }
    translate([(10 - 7.8) / 2, -7.8]) square(7.8);
}
difference() {
    translate([1.5, 22, 7.8 / 2])
        rotate([0, 90])
            cylinder(d = 3.5, h = 3 + 2 + 2);
    #translate([(10 - 1) / 2, 26 - 15.5]) cube([1, 15.5, 10]);
}
translate([-1, 0]) cube([12, 2, 7.8 + 1]);

/*
difference() {
    linear_extrude(7.8) {
        difference() {
            square([10, 26]);
            translate([(10 - 3) / 2, 26 - 15.5]) square([3, 15.5]);
        }
        translate([(10 - 7.8) / 2, -7.8]) square(7.8);
    }
    translate([1.5, 22, 7.8 / 2])
        rotate([0, 90])
            cylinder(d = 4.5, h = 3 + 2 + 2);
}
translate([-1, 0]) cube([12, 2, 7.8 + 1]);
*/