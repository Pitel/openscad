//prkynka = [20, 16, 12, 10, 8, 8, 7, 7, 7];
prkynka = [20, 8, 8, 12, 7.5, 7.5, 7.5];
space = 3.65;

prkynka = [16, 10]; // TODO: Rozdelit na pul!
//prkynka = [5, 5]; // TODO: Rozdelit na pul!
space = 20;

function add(v, i = 0, r = 0) = i < len(v) ? add(v, i + 1, r + v[i]) : r;
function take(v, n) = [ for (i = [0 : n - 1]) v[i] ];
    
edge = add(prkynka) + (len(prkynka) + 1) * space;

difference() {
    /*scale([1, 1, 0.5])*/ rotate([-90]) cylinder(d=edge, h=edge);
    translate([-edge / 2, 0, -edge]) cube(edge);
    translate([-edge / 2, space/*, space / 2*/]) {
        for (i = [0 : len(prkynka) - 1]) {
            #translate([0, i * space + add(take(prkynka, i))])
                //cube([edge, prkynka[i], edge / 4]);
                translate([edge / 2, 0, 100])
                    rotate([-90])
                        cylinder(h = prkynka[i], d=200);
        }
    }
}