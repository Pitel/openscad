txt = "PAPÍR";
size = 30;
font = "Ubuntu:style=Bold";

union() {
    linear_extrude(2) text(txt, size, font);
    
    linear_extrude(0.4) offset(1) hull() text(txt, size, font);
}