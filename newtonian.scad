difference() {
    union() {
        import("Front.stl");
        import("Mount.stl");
        import("Backblock.stl");
    }
    translate([0, 0, -100])
        for (a = [60 : 60 : 360]) {
            rotate([0, 0, a])
            translate([82, 0, 0])
            cylinder(d=10, h=1000, $fs=0.6);
        }
}