difference() {
    resize([0, 0, 150], true) translate([0, 0, -2]) import("Krone.stl");
    #for(r = [0 : 90 : 360]) {
        rotate(r) translate([0, -50]) rotate([45]) cylinder(d=3, h=50, center = true);
    }
}