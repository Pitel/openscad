bed = 22.75;
wall = bed / 8;

difference() {
    cube([100, 2 * bed + 3 * wall,  2 * bed + wall]);
    translate([0, wall, 0]) cube([100, bed, 2 * bed]); // bed
    translate([wall, bed + 2 * wall, wall]) cube([100 - 2 * wall, bed, 2 * bed]); // pocket
    cube([100, wall, 2 * bed - wall - 2 * wall]); // short
}