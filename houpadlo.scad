linear_extrude(50) difference() {
    circle(180, $fn=180);
    square(sqrt(2*pow(180, 2)), true);
}